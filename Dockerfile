FROM openjdk:11-alpine
ENTRYPOINT ["/usr/bin/grogo.sh"]

COPY grogo.sh /usr/bin/grogo.sh
COPY target/grogo.jar /usr/share/grogo/grogo.jar
